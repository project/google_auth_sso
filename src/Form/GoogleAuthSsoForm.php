<?php

namespace Drupal\google_auth_sso\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth_google\Form\GoogleAuthSettingsForm;

/**
 * Settings form for Social Auth Google.
 */
class GoogleAuthSsoForm extends GoogleAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('social_auth_google.settings');

    $form['google_settings']['advanced']['restricted_ips'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Restricted IPs'),
      '#default_value' => $config->get('restricted_ips'),
      '#description' => $this->t('If you want to restrict the users to specific ips, insert your ips here (commas separated). For example 192.158.1.38,195.128.1.38.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    $this->config('social_auth_google.settings')
      ->set('restricted_ips', $values['restricted_ips'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
