<?php

namespace Drupal\google_auth_sso\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to user/login/google .
 */
class GoogleAuthSsoAccessCheck implements AccessInterface {

  /**
   * Checks access if ip is allowed for access to user/login/google.
   */
  public function access(AccountInterface $account) {
    $client_ip = \Drupal::request()->getClientIp();
    $restricted_ips = \Drupal::config('social_auth_google.settings')->get('restricted_ips');

    if ($restricted_ips) {
      if (!in_array($client_ip, explode(',', $restricted_ips))) {
        return AccessResult::forbidden();
      }
    }
    return AccessResult::allowed();
  }

}
