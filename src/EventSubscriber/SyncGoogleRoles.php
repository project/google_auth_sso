<?php

namespace Drupal\google_auth_sso\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\AuthManager\OAuth2ManagerInterface;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\Event\UserEvent;
use Drupal\social_auth\SettingsTrait;
use Drupal\social_auth\SocialAuthDataHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Manage "drupal roles" based on "google drupal roles".
 *
 * This example shows how to use the OAuth2 Manager provided by Social Auth
 * implementers to request more data.
 *
 * @package Drupal\google_auth_sso\EventSubscriber
 */
class SyncGoogleRoles implements EventSubscriberInterface {

  use SettingsTrait;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The data handler.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  protected $dataHandler;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  protected $networkManager;

  /**
   * The provider auth manager.
   *
   * @var \Drupal\social_auth\AuthManager\OAuth2ManagerInterface
   */
  protected $providerAuth;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SocialAuthSubscriber constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   Used to manage session variables.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of the social auth implementer network plugin.
   * @param \Drupal\social_auth\AuthManager\OAuth2ManagerInterface $provider_auth
   *   Used to get the provider auth manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Used for accessing Drupal configuration.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   */
  public function __construct(MessengerInterface $messenger,
                              SocialAuthDataHandler $data_handler,
                              NetworkManager $network_manager,
                              OAuth2ManagerInterface $provider_auth,
                              ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager) {

    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->dataHandler = $data_handler;
    $this->networkManager = $network_manager;
    $this->providerAuth = $provider_auth;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Returns an array of event names this subscriber wants to listen to.
   * For this case, we are going to subscribe for user login event and call the
   * methods to react on these events.
   */
  public static function getSubscribedEvents() {
    $events[SocialAuthEvents::USER_LOGIN] = ['onUserLogin'];
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];

    return $events;
  }

  /**
   * Auto-activate new user based on "google drupal roles".
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The Social Auth user event object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onUserCreated(UserEvent $event) {

    // If user creation requires "administrator approval".
    if ($this->isApprovalRequired()) {

      // Get google drupal roles.
      $google_drupal_roles = $this->getGoogleDrupalRoles($event);

      if (!empty($google_drupal_roles)) {

        /**
         * @var Drupal\user\UserInterface $user
         */
        $user = $event->getUser();
        $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple($google_drupal_roles);
        // Add roles from google.
        foreach ($roles as $role) {
          $user->addRole($role->id());
        }
        $user->activate();
        $user->save();

        $link = Url::fromRoute('social_auth_google.redirect_to_google')->toString();
        $markup = new TranslatableMarkup('Your account has been created. It has been activated automatically because of your google informations.<br/>
            You have to <a href="@link">log in again</a>.', ['@link' => $link]);

        $this->messenger->addStatus($markup);
      }
    }
  }

  /**
   * Retrieve "google drupal roles".
   *
   * Retrieve "google drupal roles on each user login
   * and update "drupal roles" according to.
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The Social Auth user event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function onUserLogin(UserEvent $event) {

    // Get google drupal roles.
    $google_drupal_roles = $this->getGoogleDrupalRoles($event);

    $this->messenger->addStatus('Google drupal Roles: ' . implode(', ', $google_drupal_roles));

    /**
     * @var Drupal\user\UserInterface $user
     */
    $drupal_user = $event->getUser();

    // Remove all existing roles.
    $old_roles = $drupal_user->getRoles(TRUE);
    foreach ($old_roles as $role) {
      $drupal_user->removeRole($role);
    }
    $this->messenger->addStatus('Removed old Roles: ' . implode(', ', $old_roles));

    // Try to load drupal roles based on google roles.
    // "google drupal roles" without correlation with "drupal roles".
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple($google_drupal_roles);
    // Add roles from google.
    foreach ($roles as $role) {
      $drupal_user->addRole($role->id());
    }
    $drupal_user->save();

    $this->messenger->addStatus('Added drupal Roles: ' . implode(', ', $drupal_user->getRoles()));
  }

  /**
   * Get "google drupal roles" based on current google user.
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The event.
   *
   * @return array
   *   Drupal roles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  private function getGoogleDrupalRoles(UserEvent $event) {

    // Sets prefix.
    $this->dataHandler->setSessionPrefix($event->getPluginId());

    // Gets client object.
    $client = $this->networkManager->createInstance($event->getPluginId())->getSdk();

    // Create provider OAuth2 manager.
    // Can also use $client directly and request data using the library/SDK.
    $this->providerAuth->setClient($client)
      ->setAccessToken($this->dataHandler->get('access_token'));

    // Get user info.
    $userInfo = $this->providerAuth->getUserInfo();

    $data = $this->providerAuth->requestEndPoint(
      'GET',
      '/admin/directory/v1/users/' . urlencode($userInfo->getEmail()) .
      '?' . http_build_query([
        'projection' => 'full',
        'viewType' => 'domain_public',
      ])
    );

    $google_drupal_roles = [];
    if (!empty($data['customSchemas']['Drupal']['Roles'])) {

      foreach ($data['customSchemas']['Drupal']['Roles'] as $google_role) {
        $google_drupal_roles[] = $google_role['value'];
      }
    }

    return $google_drupal_roles;
  }

}
