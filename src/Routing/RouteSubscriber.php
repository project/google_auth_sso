<?php

namespace Drupal\google_auth_sso\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    if ($route = $collection->get('social_auth_google.settings_form')) {
      $route->setDefault('_form', 'Drupal\google_auth_sso\Form\GoogleAuthSsoForm');
    }

    // Custom access check for allowed ips.
    if ($route = $collection->get('social_auth_google.redirect_to_google')) {
      $route->setRequirement('_custom_access', 'Drupal\google_auth_sso\Access\GoogleAuthSsoAccessCheck::access');
    }

  }

}
