# GOOGLE AUTH SSO

This module is used to allow any google member to connect to a Drupal site.  
Based on "[GSuite User Custom Fields](https://support.google.com/a/answer/6208725?hl=en)"
this module will try to sync roles to the newly created user and at each user login.

## INTRODUCTION

* For a full description of the module, visit the project page:
  https://drupal.org/project/google_auth_sso
* To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/google_auth_sso

## REQUIREMENTS

* [Social Auth Google](https://drupal.org/project/social_auth_google)

## RECOMMENDED MODULES

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

Then go to `/admin/config/social-api/social-auth/google` in order to configure google OAuth settings.  
Add this scope `https://www.googleapis.com/auth/admin.directory.user.readonly` in the field "Scopes for API call" (under Advanced settings).  
For more information : https://www.drupal.org/docs/8/modules/social-api/social-api-2x/social-auth-2x/social-auth-google-2x-installation

This module depend on this configuration `/admin/config/people/accounts` :  
`Registration and cancellation > Who can register accounts > Visitors, but administrator approval required`.

## CONFIGURATION

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

## MAINTAINERS

This project has been sponsored by:

[Insign](https://www.drupal.org/insign)
